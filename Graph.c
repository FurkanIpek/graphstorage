#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <limits.h>

#include "Graph.h"

#define EPSILON 0.000001

int net_depth;
int imbalance;

void InitGraphData(Graph* graph)
{
	int k;
	
	imbalance = 5;
	net_depth = 4;

	if (graph->partition_on == ROW_NET || graph->partition_on == COL_NET)
	{
		// init for row net part
		graph->x_pins = (int*)malloc(graph->NZ * sizeof(int));
		graph->r_nets = (int*)malloc((graph->I + 1) * sizeof(int));
		graph->r_nets[graph->I] = graph->NZ;
		graph->rn_val = (double*)malloc(graph->NZ * sizeof(double));
		graph->rn_vrtx_wghts = (int*)calloc(graph->J, sizeof(int));
		// init for col net part
		graph->y_pins = (int*)malloc(graph->NZ * sizeof(int));
		graph->c_nets = (int*)malloc((graph->J + 1) * sizeof(int));
		graph->c_nets[graph->J] = graph->NZ;
		graph->cn_val = (double*)malloc(graph->NZ * sizeof(double));
		graph->cn_vrtx_wghts = (int*)calloc(graph->I, sizeof(int));
	}
	else if (graph->partition_on == TWO_D)
	{
		// init for 2d part
		graph->td_pins = (Triple*)malloc(graph->NZ * sizeof(Triple));
		// TODO td_nets is inefficient, real-time 2D storage of matrix - should not be used. partition does not use it, kept it just in case i might need it
		graph->td_nets = (double**) malloc(graph->I * sizeof(double*));
		for (k = 0; k < graph->I; k++)
			graph->td_nets[k] = (double*) calloc(graph->J, sizeof(double));
		graph->td_vrtx_wghts = (int*)calloc(graph->NZ, sizeof(int));
	}

}

void SetParams(int imbalance, int depth)
{
	imbalance = imbalance;
	net_depth = depth;
}

void RowNet(Graph* graph, const int i, const int j, const double w)
{
	static long int cur_row_ptr = 0,
				cur_col_ptr = 0;

	if (cur_row_ptr == 0 || cur_row_ptr - 1 != i)
		graph->r_nets[cur_row_ptr++] = cur_col_ptr;

	graph->x_pins[cur_col_ptr] = j;

	graph->rn_val[cur_col_ptr++] = w;

	graph->rn_vrtx_wghts[j]++;
}

void ColNet(Graph* graph, const int i, const int j, const double w)
{
	static long int cur_row_ptr = 0,
		cur_col_ptr = 0;

	if (cur_col_ptr == 0 || cur_col_ptr - 1 != j)
		graph->c_nets[cur_col_ptr++] = cur_row_ptr;

	graph->y_pins[cur_row_ptr] = i;

	graph->cn_val[cur_row_ptr++] = w;

	graph->cn_vrtx_wghts[i]++;
}

void TwoD(Graph* graph, const int i, const int j, const double w)
{
	static int index = 0;
	graph->td_nets[i][j] = w;

	graph->td_pins[index].i = i;
	graph->td_pins[index].j = j;
	graph->td_pins[index].w = w;
	// TODO for 2D, every vertex has 2 net connections, right?
	graph->td_vrtx_wghts[index] = 2;
	index++;
}

int compareRow(const void* c1, const void* c2)
{
	Triple* e1 = (Triple*)c1;
	Triple* e2 = (Triple*)c2;

	if (e1->i > e2->i)
		return 1;

	else if (e1->i < e2->i)
		return -1;

	else
	{
		if (e1->j > e2->j)
			return 1;

		else if (e1->j < e2->j)
			return -1;

		else return 0;
	}
}

int compareCol(const void* c1, const void* c2)
{
	Triple* e1 = (Triple*)c1;
	Triple* e2 = (Triple*)c2;

	if (e1->j > e2->j)
		return 1;

	else if (e1->j < e2->j)
		return -1;

	else
	{
		if (e1->i > e2->i)
			return 1;

		else if (e1->i < e2->i)
			return -1;

		else return 0;
	}
}

void ReadAndConvert(Graph* graph, FILE* file_in)
{
	int i, j, k, count = 0;
	long double w;

	Triple* input = (Triple*) malloc(graph->NZ * sizeof(Triple));

	if (input == NULL)
	{
		printf("Failed to allocate\n");
		return;
	}
	
	for (k = 0; k < graph->NZ; k++)
	{
		if (fscanf(file_in, "%d %d", &i, &j) > 0)
		{
			// MM coordinates are 1 based, convert them to 0 base
			--i;	--j;

			if (mm_is_pattern(graph->matcode))
				w = 1;
			else
				fscanf(file_in, "%Lf", &w); 

			input[k].i = i;
			input[k].j = j;
			input[k].w = w; 
			count++;

			if (mm_is_symmetric(graph->matcode) && i != j)
			{
				input[++k].i = j;
				input[k].j = i;
				input[k].w = w; 
				count++;
			}
		}
		else break;
	}

	graph->NZ = count;
	InitGraphData(graph);

	// sort input array on row index
	if (graph->partition_on == ROW_NET || graph->partition_on == COL_NET)
		qsort(input, graph->NZ, sizeof(Triple), &compareRow);
	
	for (k = 0; k < graph->NZ; k++)
	{
		if (graph->partition_on == ROW_NET || graph->partition_on == COL_NET)
			RowNet(graph, input[k].i, input[k].j, input[k].w);
		if (graph->partition_on == TWO_D)
			TwoD(graph, input[k].i, input[k].j, input[k].w);
	}
	
	if (graph->partition_on == ROW_NET || graph->partition_on == COL_NET)
	{
		// sort input array on column index
		qsort(input, graph->NZ, sizeof(Triple), &compareCol);

		for (k = 0; k < graph->NZ; k++)
			ColNet(graph, input[k].i, input[k].j, input[k].w);
	}

	free(input);
}

int LoadFile(Graph* graph, STORAGE partition_on, char* file_name)
{
	FILE* file_in;
	MM_typecode matcode;
	int i, j, nz;

	if ((file_in = fopen(file_name, "r")) == 0)
		return 0;

	if (mm_read_banner(file_in, &matcode) != 0)
		return 0; // wrong file format

	mm_read_mtx_crd_size(file_in, &i, &j, &nz);
	graph->partition_on = partition_on;
	graph->I = i;
	graph->J = j;
	graph->NZ = nz;
	if (mm_is_symmetric(matcode)) graph->NZ *= 2;
	graph->parted = 0;
	for (i = 0; i < 4; i++)
		graph->matcode[i] = matcode[i];
	// malloc initializations are done after reading file, via ReadAndConvert function
	ReadAndConvert(graph, file_in);
	fclose(file_in);
	
	return 1;
}

void FreeParts(Part* part)
{
	int i;

	free(part->clstr_wghts);
	for (i = 0; i < net_depth; i++)
		free(part->net_owners[i]);
	free(part->net_owners);
	free(part->records);
	free(part->calculator);
	free(part);
}

void g_Free(Graph* graph)
{
	int i;

	if (graph->partition_on == ROW_NET || graph->partition_on == COL_NET)
	{
		free(graph->x_pins);
		free(graph->r_nets);
		free(graph->rn_val);
		free(graph->rn_vrtx_wghts);

		if (graph->parted == 1 && graph->partition_on == ROW_NET)
			FreeParts(graph->rn_part);

		free(graph->y_pins);
		free(graph->c_nets);
		free(graph->cn_val);
		free(graph->cn_vrtx_wghts);

		if (graph->parted == 1 && graph->partition_on == COL_NET)
			FreeParts(graph->cn_part);
	}
	else if (graph->partition_on == TWO_D)
	{
		free(graph->td_pins);
		for (i = 0; i < graph->I; i++)
			free(graph->td_nets[i]);
		free(graph->td_nets);
		free(graph->td_vrtx_wghts);
	
		if (graph->parted == 1)
			FreeParts(graph->td_part);
	}
}

int FindNextNet(int* nets, int* pins, int cur_vertex, int reset)
{
	// updated, now finds nets in constant time by leveraging the fact that for row-net, col-net part(and vice versa)
	// hold the information about which nets a vertex has an edge to
	int net = -1, l1, l2, l3;
	static int ptr = 0, cur_vtx = -1;

	// reset when vertex changes or by demand
	if (cur_vtx != cur_vertex || reset == 1)
	{
		ptr = 0;
		cur_vtx = cur_vertex;
	}

	if (ptr < nets[cur_vertex + 1] - nets[cur_vertex])
		net = pins[nets[cur_vertex] + ptr++];

	if (net < -1)
		net = -1;
	
	return net;
}

void InitPartVars(Part* part, int tot_vrtx_wght, int num_vertices, int num_nets, int num_clusters, int* random_vertex_order)
{
	int i,j, temp;
	/* Create a random order of vertices */
	for (i = 0; i < num_vertices; i++)
		random_vertex_order[i] = i;

	for (i = 0; i < num_vertices; i++)
	{
		j = rand() % num_vertices;
		temp = random_vertex_order[i];
		random_vertex_order[i] = random_vertex_order[j];
		random_vertex_order[j] = temp;
	}
	/************************************/
	(*part).num_clusters = num_clusters;
	(*part).size_of_a_cluster = (tot_vrtx_wght / num_clusters) 
		+ ceil(((double)(imbalance * tot_vrtx_wght) / (double)num_clusters) / 100.0);
	(*part).clstr_wghts = (int*) calloc(num_clusters, sizeof(int));
	(*part).records = (int*) malloc(num_vertices * sizeof(int));
	memset((*part).records, -1, num_vertices * sizeof(int));
	(*part).net_owners = (int**) malloc(num_nets * sizeof(int*));
	for (i = 0; i < num_nets; i++)
	{
		(*part).net_owners[i] = (int*)malloc(net_depth * sizeof(int));
		memset((*part).net_owners[i], -1, net_depth * sizeof(int));
	}
	(*part).calculator = (int*) malloc(num_nets * sizeof(int)); // 1 storage for every net
}

void CalculateMetrics(Part* part, int* nets, int* pins, int num_nets)
{
	int i, j, cur_cluster, cur_net, cur_vtx, lambda = 0, tot_vol = 0;
	int* seen = (int*) malloc(part->num_clusters * sizeof(int));

	// for every net
	for (cur_net = 0; cur_net < num_nets; cur_net++)
	{
		// loop will record connected clusters, via the current net
		memset(seen, 0, part->num_clusters * sizeof(int));
		// for every vertex in the current net
		for (j = nets[cur_net]; j < nets[cur_net + 1]; j++)
		{
			cur_vtx = pins[j];
			// find which cluster this vertex belongs to
			cur_cluster = part->records[pins[j]];
			// and update "seen" array of that cluster
			if (seen[cur_cluster] == 0)
			{
				seen[cur_cluster] = 1;
				// count how many clusters are connected to this net
				++lambda;
			}
		}
		// save connectivity-1 metric for every net to calculator array
		part->calculator[cur_net] = lambda - 1;
		tot_vol += lambda - 1;
		lambda = 0;
	}
	
	part->metrics[TOT_VOL] = tot_vol;
	free(seen);
}

int Distribute(Graph* graph, int* random_vertex_order, int num_clusters, int num_vertices)
{
	int i, c, cur_vertex, cur_cluster, cur_net;
	// NOTE!! works only for row net so far
	for (c = 0; c < num_clusters * 10 && c < num_vertices; c++)
	{
		cur_vertex = random_vertex_order[c];
		cur_cluster = c % num_clusters;
		cur_net = FindNextNet(graph->c_nets, graph->y_pins, cur_vertex, 1);

		while (cur_net != -1)
		{
			for (i = 0; i < net_depth; i++)
			{
				if (graph->rn_part->net_owners[cur_net][i] == -1)
				{
					graph->rn_part->net_owners[cur_net][i] = cur_cluster;
					break;
				}
			}
			cur_net = FindNextNet(graph->c_nets, graph->x_pins, cur_vertex, 0);
		}

		graph->rn_part->records[cur_vertex] = cur_cluster;
		graph->rn_part->clstr_wghts[cur_cluster] += 1;//graph->rn_vrtx_wghts[cur_vertex];
	}

	return c;
}

int AssignRandomCluster(int* clstr_wghts, int num_clusters, int size_of_a_cluster)
{
	int cluster = rand() % num_clusters;
	int num_tries = 0;
	
	while (clstr_wghts[cluster] + 1 > size_of_a_cluster && num_tries < 2*num_clusters)
	{
		cluster = rand() % num_clusters;
		++num_tries;
	}
	//no luck with random, just find an open spot
	for (cluster = 0; cluster < num_clusters; cluster++)
	{
		if (clstr_wghts[cluster] + 1 <= size_of_a_cluster)
			break;
	}
		
	return cluster;
}

int LightestCluster(int vrtx_weight, int num_clusters, int size_of_a_cluster, int* clstr_wghts)
{
	int val = INT_MAX, min = -1, i;
	
	for (i = 0; i < num_clusters; i++)
	{
		if (clstr_wghts[i] < val && clstr_wghts[i] + vrtx_weight <= size_of_a_cluster)
		{
			min = i;
			val = clstr_wghts[i];
		}
	}

	return min;
}

/*
vertex = current vertex being evaluated to partition, vrtx_weight = weight of the current vertex
part = part that being partitioned(row-net or col-net or td-net)
weights = weights of the nets over the clusters(values are calculated depending on the current vertex)
nets & pins = for every net, pins vector holds the pins connected to that net, see compressed row storage
nets2 & pin2 = same as the above, yet in every call, if the first couple is for row-net, second couple is for col-net and vice-versa
	this second couple is being used for determinining which nets a vertex has an edge to in constant time.
*/
int AssignCluster(int vertex, int vrtx_weight, Part* part, int* weights, int* nets, int* pins, int* nets2, int* pins2)
{
	int i, cur_net, cur_cluster, best_cluster = -1, max = -1;

	// calculate how many neighbors "vertex" has at each cluster
	while ((cur_net = FindNextNet(nets2, pins2, vertex, 0)) != -1)
	{
		for (i = 0; i < net_depth; i++)
		{
			cur_cluster = part->net_owners[cur_net][i];

			if (cur_cluster == -1) break;

			++weights[cur_cluster];

			if (weights[cur_cluster] > max && part->clstr_wghts[cur_cluster] + vrtx_weight <= part->size_of_a_cluster)
			{
				max = weights[cur_cluster];
				best_cluster = cur_cluster;
			}
		}
	}
	// if cannot find a cluster by calculating neighbor population, pick the lightweight-cluster
	if (best_cluster == -1)
		best_cluster = LightestCluster(vrtx_weight, part->num_clusters, part->size_of_a_cluster, part->clstr_wghts);

	// assign cur_cluster as an owner to the all nets which "vertex" has an edge to
	cur_net = FindNextNet(nets2, pins2, vertex, 1);
	while (cur_net != -1)
	{
		for (i = 0; i < net_depth; i++)
		{
			if (part->net_owners[cur_net][i] == -1)
			{
				part->net_owners[cur_net][i] = best_cluster;
				break;
			}
		}
		cur_net = FindNextNet(nets2, pins2, vertex, 0);
	}

	return best_cluster;
}

void CheckForReplacement(Part* part, int* nets, int* pins, int* vrtx_wghts, int num_nets, int num_vertices)
{
	int i, j, k, cur_cluster, cur_net, cur_vtx;
	int strong_clstr, val, num_str_clstr = 3;
	int* weights = (int*) malloc(part->num_clusters * sizeof(int)), 
	   * str_clstrs = (int*) malloc(num_str_clstr * sizeof(int));

	int count_miss = 0;

	// for every net
	for (cur_net = 0; cur_net < num_nets; cur_net++)
	{
		// TODO sadece kullanılcak kadarını sıfırla
		memset(weights, 0, part->num_clusters * sizeof(int));
		memset(str_clstrs, -1, num_str_clstr * sizeof(int));
		// for every vertex in the current net
		for (j = nets[cur_net]; j < nets[cur_net + 1]; j++)
		{
			cur_vtx = pins[j];
			// find which cluster this vertex belongs to
			cur_cluster = part->records[cur_vtx];
			// and update weight value of that cluster
			++weights[cur_cluster];
			// find the strong clusters of this net
			// these clusters will be used to migrate vertices from "weak" clusters
			for (i= 0; i < num_str_clstr; i++)
			{
				if (str_clstrs[i] == -1)
				{
					str_clstrs[i] = cur_cluster;
					break;
				}
				else if (weights[cur_cluster] > weights[str_clstrs[i]])
				{
					for (k = num_str_clstr - 1; k > i; k--)
					{
						str_clstrs[k] = str_clstrs[k - 1];
					}
					str_clstrs[i] = cur_cluster;
					break;
				}
			}
		}
		// now, detect the weak clusters and move vertices from weak ones to strong ones
		// hopefully, this will decrease total volume
		for (cur_cluster = 0; cur_cluster < part->num_clusters; cur_cluster++)
		{
			// TODO her vrtex sadece 1 kere hareket etsin bu fonksiyonda - locking mechanism
			// this means we have small number of vertices in this cluster which reside in cur_net
			if (weights[cur_cluster] == 1)
			{
				// for every vertex residing in this net
				for (j = nets[cur_net]; j < nets[cur_net + 1]; j++)
				{
					cur_vtx = pins[j];
					// check if this vertex is in the current WEAK cluster
					if (part->records[cur_vtx] == cur_cluster)
					{
						// yup, this one should be moved to one of strong clusters
						strong_clstr = -1;
						// look for a good replacement
						for (i = 0; i < num_str_clstr; i++)
						{
							if (str_clstrs[i] == -1 || weights[str_clstrs[i]] == 0) break;

							if (part->clstr_wghts[str_clstrs[i]] + 1/*vrtx_wghts[cur_vtx]*/ <= part->size_of_a_cluster)
							{
								strong_clstr = str_clstrs[i];
								break;
							}
						}
						if (strong_clstr != -1)
						{
							part->clstr_wghts[cur_cluster] -= 1;//vrtx_wghts[cur_vtx];
							part->clstr_wghts[strong_clstr] += 1;//vrtx_wghts[cur_vtx];
							part->records[cur_vtx] = strong_clstr;
						}
						else ++count_miss;
					}
				}
			}
		}
	}
	//printf("\tNumber of misses: %i\n", count_miss);
	free(weights);
	free(str_clstrs);
}

double PartitionUsingRowNet(Graph* graph, int num_clusters)
{
	clock_t begin, end;
	int cur_cluster, cur_vertex, cur_vrtx_wght, i,
		num_nets = graph->I,
		num_vertices = graph->J;
	int* random_vertex_order = (int*) malloc(num_vertices * sizeof(int));
	int* weights = (int*) calloc(num_clusters, sizeof(int));
	Part* part = graph->rn_part;

	InitPartVars(part, graph->I/*graph->NZ*/, num_vertices, num_nets, num_clusters, random_vertex_order);

	begin = clock();

	for (i = 0; i < num_vertices; i++)
	{
		memset(weights, 0, num_clusters * sizeof(int));

		cur_vertex = random_vertex_order[i];
		cur_vrtx_wght = 1;//graph->rn_vrtx_wghts[cur_vertex]; // NOTE!! -> change in Distribute func as well
		cur_cluster = AssignCluster(cur_vertex, cur_vrtx_wght, part, weights,
			graph->r_nets, graph->x_pins, graph->c_nets, graph->y_pins);
		//cur_cluster = AssignRandomCluster(part->clstr_wghts, num_clusters, part->size_of_a_cluster);
		
		part->records[cur_vertex] = cur_cluster;
		part->clstr_wghts[cur_cluster] += cur_vrtx_wght;
	}

	/*CalculateMetrics(part, graph->r_nets, graph->x_pins, num_nets);
	printf("Tot vol would have been %i\ntime : %f\n", part->metrics[TOT_VOL], ((double)clock() - (double)begin) / CLOCKS_PER_SEC);*/
	CheckForReplacement(part, graph->r_nets, graph->x_pins, graph->rn_vrtx_wghts, num_nets, num_vertices);
	end = clock();

	CalculateMetrics(part, graph->r_nets, graph->x_pins, num_nets);
	
	/*for (i = 0; i < num_clusters; i++)
		printf("Cluster %i : %i\n", i, part->clstr_wghts[i]);

	printf("Size of a cluster: %i\n", part->size_of_a_cluster);*/

	/*printf("Parts:\n");
	for (i = 0; i < num_vertices; i++)
		printf("\t%i %i\n", i, part->records[i]);*/

	free(weights);
	free(random_vertex_order);

	return ((double)end - (double)begin) / CLOCKS_PER_SEC;
}

double PartitionUsingColNet(Graph* graph, int num_clusters)
{
	clock_t begin, end;
	int i, cur_cluster,
		num_nets = graph->J,
		num_vertices = graph->I;
	int* random_vertex_order = (int*) malloc(num_vertices * sizeof(int));
	Part* part = graph->cn_part;

	InitPartVars(part, graph->NZ, num_vertices, num_nets, num_clusters, random_vertex_order);

	begin = clock();
	for (i = 0; i < graph->J; i++)
	{
		cur_cluster = LightestCluster(1, num_clusters, part->size_of_a_cluster, part->clstr_wghts);

		part->records[i] = cur_cluster;
	}
	end = clock();
	CalculateMetrics(part, graph->c_nets, graph->y_pins, num_nets);

	return ((double)end - (double)begin) / CLOCKS_PER_SEC;
}

void CalculateMetricsFor2D(Graph* graph)
{
	int i, j, cur_net_owner, lambda_1 = 0, tot_vol = 0;
	// read row by row
	for (i = 0; i < graph->I; i++)
	{
		cur_net_owner = graph->td_part->net_owners[i][0];
		for (j = 0; j < graph->J; j++)
		{
			// if there is a vertex in (i, j) and it is connected to some other net which is located at some other cluster
			if (fabs(graph->td_nets[i][j]) <= EPSILON && graph->td_part->net_owners[graph->I + j][0] != cur_net_owner)
			{
				// we have a connectivity here
				lambda_1++;
			}
		}
		tot_vol += lambda_1 - 1;
		lambda_1 = 0;
	}
	// read column by column
	for (j = 0; j < graph->J; j++)
	{
		cur_net_owner = graph->td_part->net_owners[graph->I + j][0];
		for (i = 0; i < graph->I; i++)
		{
			// if there is a vertex in (i, j) and it is connected to some other net which is located at some other cluster
			if (fabs(graph->td_nets[i][j]) <= EPSILON && graph->td_part->net_owners[i][0] != cur_net_owner)
			{
				// we have a connecticity here
				lambda_1++;
			}
		}
		tot_vol += lambda_1 - 1;
		lambda_1 = 0;
	}
	graph->td_part->metrics[TOT_VOL] = tot_vol;
}

double PartitionUsing2D(Graph* graph, int num_clusters)
{
	clock_t begin, end;
	int i, cur_cluster, 
		num_nets = graph->I + graph->J,
		num_vertices = graph->NZ;
	int* random_vertex_order = (int*)malloc(num_vertices * sizeof(int));

	InitPartVars(graph->td_part, graph->NZ, num_vertices, num_nets, num_clusters, random_vertex_order);

	begin = clock();
	for (i = 0; i < graph->NZ; i++)
	{
		cur_cluster = LightestCluster(1, num_clusters, graph->td_part->size_of_a_cluster, graph->td_part->clstr_wghts);
		
		graph->td_part->records[i] = cur_cluster;
	}
	end = clock();
	CalculateMetricsFor2D(graph);
	return ((double)end - (double)begin) / CLOCKS_PER_SEC;
}

double Partition(Graph* graph, int num_clusters)
{
	double running_time = -1.0;
	// should be here for random vertex ordering
	srand(((unsigned)time((void*)0)));

	if (graph->partition_on == ROW_NET)
	{
		graph->parted = 1;
		graph->rn_part = (Part*) malloc(sizeof(Part));

		running_time = PartitionUsingRowNet(graph, num_clusters);
	}

	else if (graph->partition_on == COL_NET)
	{
		graph->parted = 1;
		graph->cn_part = (Part*)malloc(sizeof(Part));

		running_time = PartitionUsingColNet(graph, num_clusters);
	}

	else if (graph->partition_on == TWO_D)
	{
		graph->parted = 1;
		graph->td_part = (Part*)malloc(sizeof(Part));
		
		running_time = PartitionUsing2D(graph, num_clusters);
	}
	
	return running_time;
}

int GetMetric(Graph* graph, STORAGE metric_for, METRICS metric)
{
	switch (metric_for)
	{
	case ROW_NET: return graph->rn_part->metrics[metric]; break;

	case COL_NET: return graph->cn_part->metrics[metric]; break;

	case TWO_D: return graph->td_part->metrics[metric]; break;
	
	default: return -1;
	}
}
