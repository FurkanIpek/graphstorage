#ifndef GRAPH_H
#define GRAPH_H

#include "mmio.h"

typedef enum STORAGE { ROW_NET, COL_NET, TWO_D } STORAGE;
typedef enum METRICS { MSG, TOT_MSG, VOL, TOT_VOL } METRICS;

int part_depth;

typedef struct Part {
	int size_of_a_cluster, num_clusters;
	// cluster weights - how many vertices each cluster stores currently
	int* clstr_wghts;
	// record which vertex assigned to which cluster
	int* records;
	// owners of nets
	int** net_owners;
	// array for calculating metrics
	int* calculator;
	// metrics
	int metrics[4]; // 0 -> Message, 1 -> Total Msg, 2 -> Volume, 3 -> Total V
} Part;

typedef struct Triple {
	int i, j;
	double w;
} Triple;

/*
* Notes: 
* 1) rn(rown-net) cn(col-net) td(two d)
* 2) pin and vertex can be used interchangebly, ie they both mean the same
* 3) a net is a container which groups some pins together, ie all pins in one net are connected to each other through that net
*/

typedef struct Graph {
	MM_typecode matcode;
	int I, J, NZ;
	STORAGE partition_on;
	// parameters for partitioning
	int imbalance; // percentage
	int depth;
	// for row-net
	int* x_pins;
	int* r_nets;
	double* rn_val; // weights of edges
	int* rn_vrtx_wghts; // weights of vertices
	// for col-net
	int* y_pins;
	int* c_nets;
	double* cn_val; // weights of edges
	int* cn_vrtx_wghts; // weights of vertices
	// for 2D
	double** td_nets;
	Triple* td_pins;
	int* td_vrtx_wghts; // weights of vertices
	// Partition data
	Part* rn_part, * cn_part, * td_part;
	int parted;
} Graph;

int LoadFile(Graph* graph, STORAGE partition_on, char* file_name);
void SetParams(int imbalance, int depth);
double Partition(Graph* graph, int num_clusters);
int GetMetric(Graph* graph, STORAGE metric_for, METRICS metric);
void g_Free(Graph* graph);

#endif
