#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#include "Graph.h"

#define FILE_0	"mtx\\Pres_Poisson.mtx" // 348k non-zero entry
#define FILE_1	"mtx\\cage3.mtx" // xx small graph, 19 non-zeros
#define FILE_2	"mtx\\bcsstk06.mtx" // small size, 4140 non-zeros
#define FILE_3	"mtx\\belgium_osm.mtx" // 1.4m * 1.4m
#define FILE_4	"mtx\\ca2010.mtx"

int main(int argc, char* argv [])
{
	Graph* graph = (Graph*) malloc(sizeof(Graph));
	char* file_name = FILE_4;
	double run_time;
	int tot_vol,
		cluster_number = 1024,
		net_depth = 4,
		load_imbalance = 5;
		
	/*if (argc < 3)
	{
	 printf("Specify file name and cluster number");
	 return EXIT_FAILURE;
	 }
	 file_name = argv[1];
	 cluster_number = (int) atoi(argv[2]);*/

	if (argc < 4)
	{
		printf("Usage: %s <source file> <cluster_number> <net_depth> <optional_max_imbalance>\n", argv[0]);
		return EXIT_FAILURE;
	}
	
	file_name = argv[1];
	cluster_number = (int) atoi(argv[2]);
	net_depth = (int) atoi(argv[3]);
	
	if (argc == 5)
		load_imbalance = abs((int) atoi(argv[4]));

	if (cluster_number < 2 || cluster_number > 10000)
	{
		printf("Num clusters should be higher than 1!\n");
		return EXIT_FAILURE;
	}

	if (LoadFile(graph, ROW_NET, file_name))
	{
		SetParams(load_imbalance, net_depth);
		run_time = Partition(graph, cluster_number);
		tot_vol = GetMetric(graph, ROW_NET, TOT_VOL);
		printf("%i %1.8f", tot_vol, run_time);
		g_Free(graph);
	}
	else
	{
		printf("Wrong file format or file could not be opened!\n");
		return EXIT_FAILURE;
	}

	free(graph);

	return EXIT_SUCCESS;
}
